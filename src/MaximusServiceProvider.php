<?php

namespace Maximus\ServerCreator;

use Maximus\ServerCreator\Commands\PanelCleanupCommand;
use Maximus\ServerCreator\Commands\PanelSyncCommand;
use Maximus\ServerCreator\Commands\ServerCreateCommand;
use Maximus\ServerCreator\Console\Kernel;
use Maximus\ServerCreator\Models\PanelServer;
use Maximus\ServerCreator\Observers\PanelServerObserver;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\ServiceProvider;

class MaximusServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->loadMigrationsFrom(__DIR__ . '/Migrations');
            $this->commands([
                ServerCreateCommand::class,
                PanelSyncCommand::class,
                PanelCleanupCommand::class,
            ]);

            // Cron jobs
            $this->app->booted(function () {
                $schedule = app(Schedule::class);
                $schedule->command('maximus:panel-sync')->everyFiveMinutes();
            });
        }

        PanelServer::observe(PanelServerObserver::class);
    }
}
