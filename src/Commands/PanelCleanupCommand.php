<?php

namespace Maximus\ServerCreator\Commands;


use Maximus\ServerCreator\Exceptions\AllocationNotFoundException;
use Maximus\ServerCreator\Exceptions\NodeNotFoundException;
use Maximus\ServerCreator\Panel\Panel;
use Illuminate\Console\Command;


class PanelCleanupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'maximus:panel-cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleanup command for pterodactyl';



    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $panel = new Panel();

            $panel->deleteNotExistsServers();
            $this->line('Not exists servers are deleted');

            $panel->deleteUnusedNodes();
            $this->line('Unused nodes deleted');

            $panel->deleteUnusedLocations();
            $this->line('Unused locations deleted');
//
//        $panel->syncSteamTokens();
//        $this->line('Steam ids are synced');


            $this->line('Done');
        } catch (\Exception $exception) {
            // TODO: slack notification
        }
    }
}
