<?php

namespace Maximus\ServerCreator\Observers;

use App\Jobs\DisableUserRewardJob;
use App\Jobs\PanelServerDeletingJob;
use App\Jobs\PanelServerPowerJob;
use App\Model\RewardBonus;
use App\Model\UserReward;
use Maximus\ServerCreator\Models\PanelServer;
use Maximus\ServerCreator\Panel\Panel;
use Carbon\Carbon;

class PanelServerObserver
{
    /*
    public function deleting(PanelServer $panelServer)
    {
        $job = new PanelServerDeletingJob($panelServer->server_id, $panelServer->steam_id);
        dispatch($job->onQueue('jobs'));

    }
    */
}
